#!/bin/sh
echo -n "Merge pdf.js build and web into one."
mv build/minified/web viewer
mv build/minified/build viewer
echo " Done."

echo -n "Fix relative URL's and remove defailt PDF file."
sed -i \
    -e 's+../build/+./build/+g' \
    -e 's+../web/cmaps/+./cmaps/+g' \
    -e 's+compressed.tracemonkey-pldi-09.pdf++g' \
    viewer/pdf.viewer.js
rm viewer/compressed.tracemonkey-pldi-09.pdf
echo " Done."

echo -n "Set main viewer html as `index.html`."
mv viewer/viewer.html viewer/index.html
echo " Done."