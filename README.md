# pdfjs-viewer-build

Modified build of [Mozilla's PDF.js](https://github.com/mozilla/pdf.js) with viewer.

## Changes

Changes after [pdf.js](https://github.com/mozilla/pdf.js) is built with `gulp minified`:

- Merge `build/minified/web` and `build/minified/build` into `viewer`.
- Replace relative URL's in `viewer/pdf.viewer.js` from `../` to `./`.
- Remove default pdf in `viewer/pdf.viewer.js`.
- Rename `viewer/viewer.html` to `viewer/index.html`.

For more details look for `fix.sh` in [this repository](https://gitlab.com/beehiveor/pdfjs-viewer-build).