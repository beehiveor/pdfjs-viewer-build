FROM node:12 AS build-stage

RUN npm install -g gulp-cli@2.3.0

# build pdf.js
COPY pdf.js pdf.js
WORKDIR /pdf.js
RUN npm install
RUN gulp types
RUN gulp minified

# prepare build
COPY fix.sh ./
RUN ./fix.sh

# save result
FROM scratch AS export-stage
COPY --from=build-stage /pdf.js/build/types /types/
COPY --from=build-stage /pdf.js/viewer /viewer/
